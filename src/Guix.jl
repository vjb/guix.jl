module Guix
import Pkg

function guix_packages()
    depot = unique(filter(x -> occursin("/gnu/store", x) & isdir("$x/packages"), DEPOT_PATH))
    packages = unique(reduce(cat, (readdir("$d/packages") for d in depot)))
end

function julia_packages()
    depot = unique(filter(x -> ~occursin("/gnu/store", x) & isdir("$x/packages"), DEPOT_PATH))
    packages = unique(reduce(cat, (readdir("$d/packages") for d in depot)))
end

function packages_in_both_depots()
    intersect(guix_packages(), julia_packages())
end

function check_dependencies()
    problem_packages = packages_in_both_depots()
    load_paths = filter(x -> occursin("/gnu/store", x), LOAD_PATH)
    @assert length(load_paths) == 1
    load_path = load_paths[1]

    depot_path = DEPOT_PATH[end]

    dependencies = filter(x -> x.name in problem_packages, collect(values(Pkg.dependencies())))

    for item in dependencies
        if ~occursin("/gnu/store", item.source)
            @info "$(item.name) is loaded from julia"

            v1 = item.version
            if ispath("$load_path/$(item.name)/Project.toml")
                v2 = Pkg.Types.read_project("$load_path/$(item.name)/Project.toml").version
                if v1 != v2
                    @info "upgrade $(item.name) from $v2 to $v1"
                else
                    slug1 = splitpath(item.source)[end]
                    slug2 = readdir("$depot_path/packages/$(item.name)")[1]
                    if slug1 != slug2
                        @warn "slugs are different $slug1 != $slug2"
                    else
                        @warn "but slugs are identical $slug1\nconsider removing $(item.source)"
                    end
                end
            end
        end
    end
end

function toguixname(package)
    "julia-$(lowercase(package))"
end


# julia> Base.locate_package(Base.PkgId(uid, "StaticArrays"))
# "/tmp/jl_UzwxsRc1wl/dev/StaticArrays/src/StaticArrays.jl"

function is_std(name, uid)
    pkgid = Base.PkgId(uid, name)
    loc = Base.locate_package(pkgid)
    if isa(loc, Nothing)
        return false
    end
    occursin("stdlib", loc)
end

function isin_guix(name, versionspec=nothing)
    res = rstrip(read(`guix search ^$(toguixname(name))\$`, String))
    if isempty(res)
        return false
    end
    if isa(versionspec, Nothing)
        return true
    end
    guix_version = match(r"version: (.*)", res).captures[1]
    if VersionNumber(guix_version) in versionspec
        return true
    end
    @warn "package $(toguixname(name)) is present in guix but at incompatible version"  name guix_version versionspec
    return false
end

const LICENSE = ["LICENSE.md", "LICENSE"]

function import_from_toml(tomlfile; skip=nothing)
    if isa(skip, Nothing)
        skip = String[]
    end
    project_desc = Pkg.Types.read_project(tomlfile)
    dependencies = filter(name -> ~is_std(name, project_desc.deps[name]),
                          collect(keys(project_desc.deps)))
    res = String[]
    for dep in dependencies
        if ~isin_guix(dep)
            @info "importing package" package=dep
            try
                push!(res, importer(dep, skip=skip))
            catch e
                @error "failed to import $dep" exception=(e, catch_backtrace())
                push!(res, ";; failed to import $dep")
            end
        end
    end
    push!(res, "(packages->manifest (list $(join(toguixname.(dependencies), "\n"))))")
    join(res, "\n\n")
end


function importer(package; version=nothing, skip=nothing, test=false)
    @info "entering importer" package version skip
    current_project = Pkg.project().path
    try
        project = tempname()
        Pkg.generate(project)
        Pkg.activate(project)
        Pkg.develop(package, shared=false) # advantage of develop is that we get full repo and we do not precompile

        project_desc = Pkg.Types.read_project(Pkg.Types.projectfile_path(joinpath(project, "dev", package)))
        if isa(version, Nothing)
            version = project_desc.version
        end
        ENV["GIT_DIR"] = joinpath(project, "dev", package, ".git")
        ENV["GIT_WORK_TREE"] = joinpath(project, "dev", package)
        read(`git -c advice.detachedHead=false checkout v$version`)

        dependencies = filter(name -> ~is_std(name, project_desc.deps[name]),
                              collect(keys(project_desc.deps)))
        if test
            test_dependencies = filter(name -> ~is_std(name, project_desc.extras[name]),
                                    project_desc.targets["test"])
        else
            test_dependencies = String[]
        end

        url = replace(rstrip(read(`git config remote.origin.url`, String)), ".git" => "")

        hash = rstrip(read(`guix hash -rx $(joinpath(project, "dev", package))`, String))


        license = "undefined"
        for filename in LICENSE
            fullpath = joinpath(project, "dev", package, filename)
            if ispath(fullpath)
                license_text = read(joinpath(project, "dev", package, filename), String)
                if occursin("MIT", license_text)
                    license = "expat"
                else
                    @warn "license found but unable to detect type\n$license_text"
                end
                break
            end
        end


        synopsis = "undefined"
        if occursin("github", url)
            api_url = replace(url, "github.com"=>"api.github.com/repos")
            data = read(`guix shell curl -- curl $api_url`, String)
            m = match(r"description\": \"(.*)\"", data)
            if ~isa(m, Nothing)
                synopsis = m.captures[1]
            end
        end

        description = "undefined"
        description = read(joinpath(project, "dev", package, "README.md"), String)
        if length(description) > 100
            description = "undefined readme too long"
        end

        current_dependencies = collect(values(Pkg.dependencies()))
        res = String[]

        if isa(skip, Nothing)
            skip = String[]
        end

        for dep in vcat(dependencies, test_dependencies)
            try
                if ~(dep in skip)
                    if dep in keys(project_desc.compat)
                        if ~isin_guix(dep, project_desc.compat[dep].val)
                            i = findfirst(x -> x.name == dep, current_dependencies)
                            push!(res, importer(dep, version=current_dependencies[i].version, skip=skip))
                        end
                    else
                        if ~isin_guix(dep)
                            push!(res, importer(dep, skip=skip))
                        end
                    end
                end
            catch e
                @error "failed to import dependency $dep" exception=(e, catch_backtrace())
                push!(res, ";; failed to import dependency $dep")
            end
        end

        propagated_inputs = if ~isempty(dependencies)
        """\n(propagated-inputs
        (list $(join(toguixname.(dependencies), "\n          "))))"""
        else
            ""
        end

        inputs = if ~isempty(test_dependencies)
        """\n(inputs
        (list $(join(toguixname.(test_dependencies), "\n          "))))"""
        else
            ""
        end

        args = test ? "" : "\n(arguments (list #:tests? #f))\n"


    push!(res, """(define $(toguixname(package))
    (package
        (name "$(toguixname(package))")
        (version "$version")
        (source
        (origin
        (method git-fetch)
        (uri (git-reference
                (url "$url")
                (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
            (base32 "$hash"))))
        (build-system julia-build-system)$(args)$(propagated_inputs)$(inputs)
        (home-page "$url")
        (synopsis "$synopsis")
        (description "$description")
        (license $license)))""")
        join(res, "\n\n")
    finally
        push!(skip, package)
        delete!(ENV, "GIT_DIR")
        delete!(ENV, "GIT_WORK_TREE")
        Pkg.activate(current_project)
    end
end

end # module Guix
